package main

import (
	"api_gateway/api"
	"api_gateway/api/handler"
	"api_gateway/config"
	"api_gateway/grpc/client"
	"api_gateway/pkg/logger"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	services, err := client.NewGrpcClietns(cfg)
	if err != nil {
		log.Error("error is while initializing grpc clients", logger.Error(err))
		return
	}

	h := handler.New(cfg, services, log)

	r := api.New(h)

	log.Info("server is running.....on ", logger.Any("port:", cfg.HTTPPort))
	if err = r.Run(cfg.HTTPPort); err != nil {
		log.Error("error is while running server", logger.Error(err))
	}
}
