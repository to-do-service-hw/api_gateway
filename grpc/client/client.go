package client

import (
	"api_gateway/config"
	todo_service "api_gateway/genproto/todo_service"
	"api_gateway/genproto/user_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	TodoService() todo_service.TodoServiceClient
	UserService() user_service.UserServiceClient
}

type grpcClients struct {
	todoService todo_service.TodoServiceClient
	userService user_service.UserServiceClient
}

func NewGrpcClietns(cfg config.Config) (IServiceManager, error) {
	connTodoService, err := grpc.Dial(cfg.TodoGRPCServiceHost+cfg.TodoGRPCServicePort, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		todoService: todo_service.NewTodoServiceClient(connTodoService),
		userService: user_service.NewUserServiceClient(connTodoService),
		}, nil
}

func (g *grpcClients) TodoService() todo_service.TodoServiceClient {
	return g.todoService
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}