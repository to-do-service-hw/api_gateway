package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct{
	ServiceName string
	Environment string
	HTTPPort string
	TodoGRPCServiceHost string
	TodoGRPCServicePort string
}

func Load() Config{
	if err := godotenv.Load(); err != nil{
		log.Println("error is while gotodenv loading", err.Error())
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "to-do-service-hw"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))
	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))
	cfg.TodoGRPCServiceHost = cast.ToString(getOrReturnDefault("TODO_GRPC_SERVICE_HOST", "localhost"))
	cfg.TodoGRPCServicePort = cast.ToString(getOrReturnDefault("TODO_GRPC_SERVICE_PORT", ":8001"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{}{
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}