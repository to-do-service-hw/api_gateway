package handler

import (
	"api_gateway/config"
	"api_gateway/grpc/client"
	"api_gateway/pkg/logger"
)

type Handler struct {
	cfg      config.Config
	services client.IServiceManager
	log logger.ILogger
}

func New(cfg config.Config, services client.IServiceManager, log logger.ILogger) Handler {
	return Handler{
		cfg:      cfg,
		services: services,
		log: log,
	}
}
