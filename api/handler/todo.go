package handler

import (
	pb "api_gateway/genproto/todo_service"
	user_service "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"

	"github.com/gin-gonic/gin"
)

func (h Handler) CreateTodo(c *gin.Context) {
	request := pb.CreateTodoRequest{}

	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}

	h.services.TodoService().Create(context.Background(), &request)

	c.JSON(200, request)
}


func (h Handler) CreateUser(c *gin.Context) {
	request := user_service.CreateUser{}
	if err := c.ShouldBindJSON(&request); err != nil{
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}

	h.services.UserService().Create(context.Background(), &request)

	c.JSON(200, request)
}