# protoc -I=protos \
#     --go_out=genproto --go_opt=paths=source_relative \
#     --go-grpc_out=genproto --go-grpc_opt=paths=source_relative \
#     protos/*.proto


protoc -I=protos \
    --go_out=genproto --go_opt=paths=source_relative \
    --go-grpc_out=genproto --go-grpc_opt=paths=source_relative \
    protos/todo_service.proto

protoc -I=protos \
    --go_out=genproto --go_opt=paths=source_relative \
    --go-grpc_out=genproto --go-grpc_opt=paths=source_relative \
    protos/user_service.proto
